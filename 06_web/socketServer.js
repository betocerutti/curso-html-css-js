// Libreris expressjs.com
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

// Estaticos
app.use(require('express').static(__dirname +'/'));

// Definimos el recurso o endpoint o URL
app.get('/blog', function(req, res){
    res.sendFile(__dirname + '/blog.html');
});


io.on('connection', function(socket){


  socket.on('cd1', function(msg){
    io.emit('cd1', msg);
  });
  socket.on('cc1', function(msg){
    io.emit('cc1', msg);
  });
  socket.on('cg1', function(msg){
    io.emit('cg1', msg);
  });


});

// Inicializamos el servidor
http.listen(3000, "0.0.0.0", function(){
  console.log('listening on *:3000');
});
