// Objecto vacio
var miObjeto = {};
// También se puede
var miOtroObjeto = new Object();
var a = new Array();
var punto = { x:0, y:0 };
var punto2 = { x:point.x, y:point.y+1 };
var book = {
    "Titulo principal": "El jinete palido",
    'Sub Titulo': "Gran pelicula",
    "para": "todo publico",
    author: {
        firstname: "Cleat",
        surname: "Eastwood"
    },
    guardar: function(){

    },
};



// Añadir propiedad
miObjeto.nombre = 'SuperObjeto';
console.log(miObjeto.nombre); // SuperObjeto

// Los objetos en Javascript son Arrays asociativos
miObjeto["apellido"] = "SuperLopez";
console.log(miObjeto.apellido); // SuperLopez

// También sirve para el acceso
console.log(miObjeto["nombre"]); // SuperObjeto


// para borar propiedades
delete book.author;
delete book["Titulo principal"];

// podemos recorrer las propiedades de la siguiente manera
for (propiedad in book){
    console.log(propiedad);
}
