// Sintaxis
/*
/pattern/modifiers;
*/

// Devuelve -1 si no se encuentra


// Descripcion de los modificadores
// i	Perform case-insensitive matching
// g	Perform a global match (find all matches rather than stopping after the first match)
// m	Perform multiline matching

// Este objeto valida que la cadena termine en s
var pattern = /s$/;

// también se puede definir como:
var pattern = new RegExp("s$");


// Metodos de cadena para encontrar patrones
"JavaScript".search(/script/i); // i es para indicar que queremos que nos devuelva el índice de la conicidencia


// No matter how it is capitalized, replace it with the correct capitalization
"javascript".replace(/javascript/gi, "JavaScript");


"1 plus 2 equals 3".match(/\d+/g) // returns ["1", "2", "3"]
