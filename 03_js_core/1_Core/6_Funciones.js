// funciones como variables
var saludar = function(){
    console.log("Hola");
}
saludar(); // Hola

//NOTA IMPORTANTE
// a diferencia de la asignación de valore en un objeto, donde es
// pasado por referencia, los parámetros de una función copia el valor
// a un nuevo objeto

var saludo = 'Hola';

function saludo(saludo) {
    saludo = '';
    return saludo;
}
saludar(saludo); // ''

console.log(saludo); // Hola

// A veces nos encontraremos con funciones definidas e invocads en un pasado
(function(){
    return a + b;
})(1, 2) // inmediatamente imprime 3


// Las funciones anonimas en javascript son muy populares
var miObjeto = {
    nombre: "Juan",
    edad: 25,
    clave: function(){
        return (nombre.length+edad)*nombre.length;
    },
}
