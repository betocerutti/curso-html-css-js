// Crear Array
var miArray = [];
var primos = [2, 3, 5, 7, 11];
var variado = [ 1.1, true, "a", ];

// los elementos pueden ser expresiones variadas
var base = 1024;
var tabla = [base, base+1, base+2, base+3];

// también pueden contenter objetos literales o otros array literales
var b = [[1,{x:1, y:2}], [2, {x:3, y:4}]];


// A traves del operador new
var a = new Array(10);
var a = new Array(5, 4, 3, 2, 1, "testing, testing");

// para acceder a un elementos
a[0]; // 5

// para reasignar un valor
a[0] = 'hola';

// añadir un valor al final
a.push("zero");
a.push("one", "two");

// para conocer la cantidad de elementos
a.length; // 6

// OJO: para borrar los elementos de un Array
a.length = 0;

// también como en los objetos
delete a[1];
1 in a // false

// para recorrer un Array
for (var i=0; i<a.length; i++){
    if (!a[i]) continue; // Evita los nulos, undefined o elementos inexistentes
    if (a[i] === undefined) continue; // Evita solo undefined + inexistentes
    // hacer mas cosas...
}

// podemos hacer un for in

for(var i in sparseArray) {
    var value = sparseArray[i];
    // Now do something with index and value
}


// Join
var a = [1, 2, 3];
a.join(); // => "1,2,3"
a.join(" "); // => "1 2 3"
a.join(""); // => "123"
var b = new Array(10);
b.join('-') // => '---------': una cadena de nueve guiones

// reverse
var a = [1,2,3];
a.reverse().join() // => "3,2,1" and a is now [3,2,1]

// sort
var a = new Array("banana", "cherry", "apple");
a.sort();
ar s = a.join(", "); // "apple, banana, cherry"

// concat
var a = [1,2,3];
a.concat(4, 5) // [1,2,3,4,5]
a.concat([4,5]); // [1,2,3,4,5]
a.concat([4,5],[6,7]) // [1,2,3,4,5,6,7]
a.concat(4, [5,[6,7]]) //[1,2,3,4,5,[6,7]]


// slice
var a = [1,2,3,4,5];
a.slice(0,3); //  [1,2,3]
a.slice(3); // [4,5]
a.slice(1,-1); // [2,3,4]
a.slice(-3,-2); // [3]
