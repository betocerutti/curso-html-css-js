/*
La propiedad sessionStorage nos permite acceder al objeto Storage de la sesión.

NOTA:
Si habrimos una ventana nueva del navegador, se iniciará una sesión nueva, por
lo tanto un objeto Storage será crado y los datos del anterior no serán accesibles
desde esta nueva ventana.
*/


// ------  Sintaxis ------ //

// Guardar datos en la sesion
sessionStorage.setItem('key', 'value');

// Recuperar los datos
var data = sessionStorage.getItem('key');

// Borrar los datos guardados
sessionStorage.removeItem('key');

// Borrar todos los datos de la sessionStorage
sessionStorage.clear();


// Ejemplo de como persistir los datos de un formulario si el usuario recarga
// la página:

// Obtener el casillero que queremos persistir
var field = document.getElementById("field");

// Comprobra si ya tenemos un valor de autosave
if (sessionStorage.getItem("autosave")) {
  // Restaurar el valor
  field.value = sessionStorage.getItem("autosave");
}

// Escuchar cambios el el campo
field.addEventListener("change", function() {
  // Si ocurren cambios, guardamos nuevamente los datos
  sessionStorage.setItem("autosave", field.value);
});
