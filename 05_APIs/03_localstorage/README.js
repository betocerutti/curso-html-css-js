/*

Window.localStorage

El objeto localStorage nos permite acceder a objeto local: Storage

NOTA:
Este objeto no caduca como sí lo hace el objeto sessionStorage, por lo
que debemos encargarnos de borrar los datos cuando lo creamos coneviente.

*/

// -------  Sintaxis  -------  //

// Guardar un valor
localStorage.setItem('clave', 'valor');

// Otra manera de guardar valores
localStorage.colorSetting = '#a4509b';
localStorage['colorSetting'] = '#a4509b';
localStorage.setItem('colorSetting', '#a4509b');

// borrar todo
localStorage.clear();

// NOTA:
// Hay que tener en cuenta que el objeto localStorage solo guarda cadenas
// de tal manera que si intentamos:
localStorage.setItem('precio', 20);

// lo que realmente se guardará será:
'precio'='20'




/*
    Comprobar disponibilidad vs soporte
*/

// Que el objeto esté soportado por el navegador no quiere decir que esté disponible
// para ser utilizado, ya que los navegadores ofrecen mecanismos al usiario para desconectar
// dicha funcionalidad. Lo cual puede provoca el lanzamiento de excepciones inesperadas.


// En este ejemplo podemos comprobar que ambas casuisticas son ciertas:
function storageAvailable(type) {
	try {
		var storage = window[type];
		var x = '__storage_test__';
		storage.setItem(x, x);
		storage.removeItem(x);
		return true;
	}
	catch(e) {
		return false;
	}
}

// Y así es como lo utilizaríamos
if (storageAvailable('localStorage')) {
	// Podemos utilizarlo
}
else {
	// NO esta diponible
}
